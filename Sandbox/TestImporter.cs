using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Sandbox
{
    public class TestImporter
    {
        private readonly IDatabase _redis;

        public TestImporter()
        {
            var redis = ConnectionMultiplexer.Connect(
                "yieldigo.redis.cache.windows.net:6380,password=hxMhqbGh8Y8nCB8wd0XBcyKAE7jCnKUP6LbV47pEhq8=,ssl=True,abortConnect=False");
            _redis = redis.GetDatabase();
        }

        public void Run()
        {
            Console.Out.WriteLine("creating client");

            var counter = 1;
            foreach (var i in GetValuesFromFile())
            {
                foreach (var pair in i)
                {
                    _redis.ListRightPush(pair.Key, pair.Value.ToArray());
                }
                
                Console.Out.WriteLine($"Added {counter * 4000} items ...");
                counter++;
            }
        }

        private IEnumerable<Dictionary<string, List<RedisValue>>> GetValuesFromFile()
        {
            var lineBuffer = new Dictionary<string, List<RedisValue>>();
            var cntr = 0;
            foreach (var l in File.ReadLines("test.csv"))
            {
                cntr++;
                string key;
                Row obj;
                var r = l.Split(",");
                try
                {
                    obj = new Row
                    {
                        SiteId = string.IsNullOrEmpty(r[0]) ? 0 : Convert.ToInt64(r[0]),
                        ArticleId = string.IsNullOrEmpty(r[1]) ? 0 : Convert.ToInt64(r[1]),
                        Quantity = string.IsNullOrEmpty(r[2]) ? 0 : Convert.ToDecimal(r[2]),
                        Date = r[3],
                        Price = string.IsNullOrEmpty(r[4]) ? 0 : Convert.ToDecimal(r[4]),
                        SupplierPrice = string.IsNullOrEmpty(r[5]) ? 0 : Convert.ToDecimal(r[5])
                    };
                    key = string.Format("AS:{0}-{1}", obj.ArticleId, obj.SiteId);
                }
                catch (Exception crap)
                {
                    Console.Out.WriteLine($"skipped line because {crap.Message}:\n{l}");
                    continue;
                }

                if (lineBuffer.ContainsKey(key) == false)
                {
                    lineBuffer[key] = new List<RedisValue>();
                }

                lineBuffer[key].Add(JsonConvert.SerializeObject(obj));

                if (cntr < 4000) continue;
                
                cntr = 0;
                yield return lineBuffer;
            }
        }

        private class Row
        {
            public long SiteId { get; set; }
            public long ArticleId { get; set; }
            public string Date { get; set; }
            public decimal Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal SupplierPrice { get; set; }
        }
    }
}